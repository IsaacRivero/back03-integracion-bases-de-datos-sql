package com.techuniversity.facs.facturas;

import com.techuniversity.facs.productos.ProductoRepository;
import com.techuniversity.facs.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/facturas")
public class FacturaController {
    @Autowired
    FacturaRepository facturaRepository;
    @GetMapping("/facturas")
    public Iterable<FacturaModel> getAll(@RequestParam(defaultValue = "0") double hasta) {
        if (hasta == 0) {
            return facturaRepository.findAll();
        }else {
            return facturaRepository.findByImporte(hasta);
        }
    }
    @GetMapping("/facturas/fecha")
    public Iterable<FacturaModel> getByFecha(@RequestParam(defaultValue = "") String fecha) {
        if (fecha.equals("")) {
            return facturaRepository.findAll();
        }else {
            return facturaRepository.findByFecha(fecha);
        }
    }
    @Autowired
    ProductoService productoService;

    @PostMapping("/facturas")
    public String crearFactura(@RequestParam String cliente, @RequestParam int idProducto) {
        try {
            productoService.crearFactura(cliente,idProducto);
            return "OK";
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "KO";
        }
    }
}
