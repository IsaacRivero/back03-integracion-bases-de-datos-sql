package com.techuniversity.facs.productos;

import org.springframework.data.repository.CrudRepository;

public interface ProductoRepository extends CrudRepository<ProductoModel, Integer> {

}
